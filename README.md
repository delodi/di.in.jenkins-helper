Here you find our collection of jenkins helper scripts

### postNotifySlack
sends slack message in post step of jenkins pipeline

``` groovy
@Library('github.com/sorah/jenkinsfile-slack@master') _
pipeline {
  post {
    always {
      postNotifySlack(currentBuild.result,"#yourChannel")
    }
  }
}
```

License: MIT
