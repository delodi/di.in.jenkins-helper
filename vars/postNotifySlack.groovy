def call(String buildStatus, String channel) {
  
  def status = buildStatus ?: 'SUCCESS'
  def color = '#e3e4e6'
  def statusMessage = status

  if (status == 'STARTED') {
    color = '#e3e4e6'
    statusMessage = 'Started'
  }
  if (status == 'SUCCESS') {
    color = 'good'
    statusMessage = 'Success'
  }
  if (status == 'FAILURE') {
    color = 'danger'
    statusMessage = 'FAILURE'
  }
  if (status == 'ABORTED') {
    color = 'warning'
    statusMessage = 'Aborted'
  }
  if (status == 'NOT_BUILT') {
    color = 'warning'
    statusMessage = 'Not built'
  }
  if (status == 'UNSTABLE') {
    color = 'danger'
    statusMessage = 'Unstable'
  }
  def message = "${env.JOB_NAME} <${env.RUN_DISPLAY_URL}|#${env.BUILD_NUMBER}> ${statusMessage}"
  if(status == "SUCCESS"){
    message = "$message\nURL is ${env.APP_URL}"
  }

  slackSend (channel: channel, color: color, message: message)
}
